# Customizable Atom Animation via threejs

**[LIVE EXAMPLE](https://reeceappling.gitlab.io/atom-animation-via-threejs/index.html)**

![Image of U235 Atom](images/atom.PNG)

# Usage Options

- **Download** this repository and open viewer.html in a browser
- link to the **[live copy](https://reeceappling.gitlab.io/atom-animation-via-threejs/index.html)** of the most current commit.

## Including the animation in an iframe of square size

```html
<iframe id="atomFrame" title="Inline Frame Example" width="1000" height="1000" src="atom.html"></iframe>
```

Notes:
- the viewing area must be an iframe with the id="atomFrame"
- the view-area (iframe) for the atom __**MUST**__ be square for the image to not be skewed

## Modifying the atom

atom.js has modifiable variables and constants near the top which are fairly self-explanatory

![Modifiable Atom Variables](images/modifiable.PNG)